# Miniflux
> [Miniflux](https://miniflux.app/) is a minimalist and opinionated feed reader.

This description sums up Miniflux pretty well. It's very simple to setup and
operate, yet does exactly what it's supposed to: it manages RSS feeds.

## Environment variables
Not much configuration is required by default to get things up and running.

For the database, you have the usual PostgreSQL stuff:
* `POSTGRES_DB`: database name
* `POSTGRES_USER`: main user of the database
* `POSTGRES_PASSWORD`: password for main user

For Miniflux, I used the following defaults:
* `DATABASE_URL`: this URL defines how to connect to the database. It follows
  the form
  `postgres://POSTGRES_USER:POSTGRES_PASSWORD@db/POSTGRES_DB?sslmode=disable`
* `RUN_MIGRATIONS`: this lets Miniflux auto-migrate the database when needed
  (this is especially useful during initial startup)
* `CREATE_ADMIN`: this allows us to create an admin account using env vars
* `ADMIN_USERNAME`: admin username
* `ADMIN_PASSWORD`: admin password

Miniflux has many other variables you can set, which you can find
[here](https://miniflux.app/docs/configuration.html).
