#!/usr/bin/env sh

certbot certonly --standalone -d "$MAIN_DOMAIN,$DOMAINS" --email "$EMAIL" -n --agree-tos --expand

# The original script handles the template subsitution
exec /docker-entrypoint.sh nginx -g "daemon off;"
