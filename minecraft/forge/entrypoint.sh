#!/usr/bin/env bash

# Accept the EULA
echo "eula=true" > /app/config/eula.txt

# Log the last-used version, so we know which version to use when running it again (e.g. loading the backup)
echo "${MC_VERSION}, Forge v${FORGE_VERSION}" >> /app/config/versions.txt

XMS="${XMS:-$XMX}"

# Launch the actual server
exec java \
    -Xms"${XMS}G" \
    -Xmx"${XMX:-$XMS}G" \
    -XX:+UseG1GC \
    -XX:+ParallelRefProcEnabled \
    -XX:MaxGCPauseMillis=200 \
    -XX:+UnlockExperimentalVMOptions \
    -XX:+DisableExplicitGC \
    -XX:+AlwaysPreTouch \
    -XX:G1NewSizePercent="$([ $XMS -gt 12 ] && echo 40 || echo 30)" \
    -XX:G1MaxNewSizePercent="$([ $XMS -gt 12 ] && echo 50 || echo 40)" \
    -XX:G1HeapRegionSize="$([ $XMS -gt 12 ] && echo 16 || echo 8)"M \
    -XX:G1ReservePercent="$([ $XMS -gt 12 ] && echo 15 || echo 20)" \
    -XX:G1HeapWastePercent=5 \
    -XX:G1MixedGCCountTarget=4 \
    -XX:InitiatingHeapOccupancyPercent="$([ $XMS -gt 12 ] && echo 20 || echo 15)" \
    -XX:G1MixedGCLiveThresholdPercent=90 \
    -XX:G1RSetUpdatingPauseTimePercent=5 \
    -XX:SurvivorRatio=32 \
    -XX:+PerfDisableSharedMem \
    -XX:MaxTenuringThreshold=1 \
    -Dusing.aikars.flags=https://mcflags.emc.gs \
    -Daikars.new.flags=true \
    -jar /app/"$FORGE_JAR" \
    --universe /app/worlds \
    --nogui
