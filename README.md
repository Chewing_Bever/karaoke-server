<!---

Copyright (C) 2020 Jef Roosens

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

-->


# self-hosting
# Contents
The repo contains setup guides for the following:

* Vanilla Minecraft
* Modded Minecraft using Forge
* Modded Minecraft using Fabric
* Vanilla Terraria
* Modded Terraria (coming soon)
* Firefly 3
* Nginx
* My own backup scripts

Each directory contains (or will contain) its own `README.md` to aid with the
installation of that specific setup.

# General info
This info applies to all configs.

## Docker
All the setups named above use Docker and docker-compose. If you're on a
Linux-based server, you can find both `docker` and `docker-compose` in your
package manager (do note that the Docker package might be called `docker.io`).
Otherwise, the install instructions can be found
[here](https://docs.docker.com/engine/install/).

## Configuration
Most configuration can be done using a `.env` file with a provided
`.env.example` file to start from. This means that you never have to edit the
compose files, unless you wish to deviate from the default format.

## Building the image
You can build the container image using the command `docker-compose build`.
This will build all services specified in the `docker-compose.yml` file. Any
build configuration/environment variables can be defined in a `.env` file. A
`.env.example` file is given for each configuration.

## Running the container
For running the server, we can use `docker-compose up -d`. This will start the
service in the background. You can then see any logs using
`docker-compose logs`. If you want the logs to update automatically, use
`docker-compose logs -f`.

# Why did I make this?
Well, I just wanted to put all my knowledge in one basket. this makes it easier
to manage and share with others. I spend a lot of time tweaking these configs
and figuring out how they work best (for me at least), and wanted to share this
with everyone else.
