# Build arguments
Two build arguments are required, namely `MC_VERSION` and `FABRIC_VERSION`.
The latter is the version of Fabric you wish to install. You can find the list
of versions [here](https://fabricmc.net/use/). For example, you can then set
`FABRIC_VERSION=0.6.1.51` in the `.env` file.

# Environment variables
The two possible environment variables are `XMS` and `XMX`. These specify the
initial RAM & maximum RAM usage respectively. Only `XMX` is required; `XMS` is
just set to the same value as `XMX` if not specified. You must specify them as
a number, e.g. `XMS=4`. This number represents a quantity of gigabytes.

# Mount points
There a three useful mount points defined:

* `/app/config`: this is where all server config files reside, as well as the
  mods.
* `/app/worlds`: this is where the world files are stored.

You can mount these directories somewhere in the host file system by specifying
the mount paths in the `.env` file. These can be both absolute or relative
paths.

# Other config variables
The only other config variable is `PORT`. This specifies on what port your
server will be discoverable over the internet. The default Minecraft port is
`25565`.

# Java flags
I use the Java flags defined
[here](https://aikar.co/2018/07/02/tuning-the-jvm-g1gc-garbage-collector-flags-for-minecraft/).
If you don't agree with this decision, you can change the `ENTRYPOINT` at the
end of the `Dockerfile` to the following:

```
ENTRYPOINT java \
-Xms"${XMS:-$XMX}G" \
-Xmx"${XMX}G" \
-jar fabric-server-launch.jar \
--universe /app/worlds \
--nogui
```

This will only use the flags absolutely necessary, while still allowing you to
tweak the memory variables.

# Broken symlink
You might've noticed a broken symlink called `server.jar` in your config
directory. This is an (admittedly ugly) fix for the issue where Fabric expects
the vanilla server jar to be in the current working directory. Considering a
server also has to run within its config directory, this would mean that the
vanilla jar should be in the config directory. As I wanted to keep any
version-specific data out of the config/worlds directories, I have opted to use
a symlink to the server jar in question instead. This symlink can be safely
deleted, and will just get re-created when needed.
