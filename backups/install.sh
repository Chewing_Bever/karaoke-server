#!/usr/bin/env sh

# Zip app
(cd app && zip -r ../app.zip * -x  "__pycache__/*" "**/__pycache__/*" ".vim/*" "**/.vim/*")

# Add shebang to top of file
echo "#!/usr/bin/env python3" | cat - app.zip > backup_tool
chmod a+x backup_tool

# Move executable over 
mv backup_tool /usr/local/bin

# Remove zip
rm app.zip
