# Build arguments
Two build arguments are required, namely `MC_VERSION` and `FORGE_VERSION`.  You
can find your required versions [here](https://files.minecraftforge.net/).
Then, you can specify them in the `.env` file:

```
MC_VERSION=1.16.4
FORGE_VERSION=35.1.4
```

# Environment variables
The two possible environment variables are `XMS` and `XMX`. These specify the
initial RAM & maximum RAM usage respectively. Only `XMX` is required; `XMS` is
just set to the same value as `XMX` if not specified. You must specify them as
a number, e.g. `XMS=4`. This number represents a quantity of gigabytes.

# Mount points
There a three useful mount points defined:

* `/app/config`: this is where all server config files reside, as well as the
  mods.
* `/app/worlds`: this is where the world files are stored.

You can mount these directories somewhere in the host file system by specifying
the mount paths in the `.env` file. These can be both absolute or relative
paths.

# Other config variables
The only other config variable is `PORT`. This specifies on what port your
server will be discoverable over the internet. The default Minecraft port is
`25565`.

# Java flags
I use the Java flags defined
[here](https://aikar.co/2018/07/02/tuning-the-jvm-g1gc-garbage-collector-flags-for-minecraft/).
If you don't agree with this decision, you can change the `ENTRYPOINT` at the
end of the `Dockerfile` to the following:

```
ENTRYPOINT java \
-Xms"${XMS:-$XMX}G" \
-Xmx"${XMX}G" \
-jar "$FORGE_JAR" \
--universe /app/worlds \
--nogui
```

This will only use the flags absolutely necessary, while still allowing you to
tweak the memory variables.
