import argparse
import sys
from specs import parse_specs_file


# This just displays the error type and message, not the stack trace
def except_hook(ext_type, value, traceback):
    sys.stderr.write("{}: {}\n".format(ext_type.__name__, value))

sys.excepthook = except_hook


# Define parser
parser = argparse.ArgumentParser(
    description='Backup directories and Docker volumes.')
parser.add_argument('-f', '--file', action='append', dest='file',
                    help='File containing spec definitions.')
parser.add_argument('-j', '--json', action='store_const', const=True,
                    default=False, help='Print out the parsed specs as JSON '
                                        'and exit')
parser.add_argument('spec', nargs='*',
                   help='The specs to process. Defaults to all.')

# Parse arguments
args = parser.parse_args()
specs = sum([parse_specs_file(path) for path in args.file], [])

# Filter specs if needed
if args.spec:
    specs = filter(lambda s: s.name in args.spec, specs)

# Dump parsed data as json
if args.json:
    import json
    print(json.dumps([spec.to_dict() for spec in specs], indent=4))

else:
    pass
    # Run the backups
    # for spec in specs:
    #     spec.backup()
