# Koel
[Koel](https://github.com/koel/koel) is a self-hostable music server.

# Initial setup
After launching the application for the first time, you have to run the initial
setup. This can be done using the following command:

```
docker exec -it koel_app_1 php artisan koel:init
```

This will ask you to configure the admin user etc. The location for the music
can be left as the default (`/music`). The command will error out after asking
this; this is normal. Even though an error occurred, the system still
initialized successfully.
